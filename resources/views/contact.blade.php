@extends('layouts.layout')



@section('containt')





<div class="site-wrapper-reveal">

    <!-- breadcrumb-area start -->
    <div class="about-banner-wrap banner-space bg-img" data-bg="assets/images/bg/32518060_m.jpg" style="background-size: 100%;" >
        <div class="container">
            <div class="row">
                <div class="col-lg-8 ml-auto mr-auto">
                    <div class="about-banner-content text-center">
                        <h1 class="mb-15 text-white">Contactez-nous</h1>
                        <h5 class="font-weight--normal text-white">Nous serons heureux de vous compter parmis nos client Laissez nous un message et nous y repondrons sous peu. </h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcrumb-area end -->

    <!--====================  Conact us Section Start ====================-->
    <div class="contact-us-section-wrappaer section-space--pt_100 section-space--pb_70">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 col-lg-6">
                    <div class="conact-us-wrap-one mb-30">
                        <h3 class="heading">Pour toutes informations ou requêtes, n'hésitez surtout pas à rentrer en
                            <span class="text-color-primary">contact avec nous...</span> nous serons heureux de vous
                            répondre le plus vite possible.</h3>
                        <div class="sub-heading"></div>
                    </div>
                </div>

                <div class="col-lg-6 col-lg-6">
                    <div class="contact-form-wrap">
                        <form id="contact-form" action="assets/php/mail.php" method="post">
                            <div class="contact-form">
                                <div class="contact-input">
                                    <div class="contact-inner">
                                        <input name="con_name" type="text" placeholder="Nom & Prenom *">
                                    </div>
                                    <div class="contact-inner">
                                        <input name="con_email" type="email" placeholder="Email *">
                                    </div>
                                </div>
                                <div class="contact-inner">
                                    <input name="con_subject" type="text" placeholder="Objet *">
                                </div>
                                <div class="contact-inner contact-message">
                                    <textarea name="con_message" placeholder="Laisser Votre Message"></textarea>
                                </div>
                                <div class="submit-btn mt-20">
                                    <button class="ht-btn ht-btn-md" type="submit">Envoyez</button>
                                    <p class="form-messege"></p>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--====================  Conact us Section End  ====================-->

    <!--====================  Conact us info Start ====================-->
    <div class="contact-us-info-wrappaer section-space--pb_100">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-4 col-md-6">
                    <div class="conact-info-wrap mt-30">
                        <h5 class="heading mb-20">Cocody</h5>
                        <ul class="conact-info__list">
                            <li>Cocody,Riviera Bonoumin, Abidjan Côte d'Ivoire</li>
                            <li><a href="#" class="hover-style-link text-color-primary">courriel@sytelis.ci</a></li>
                            <li><a href="#" class="hover-style-link text-black font-weight--bold">00225 77 09 68 33</a>
                            </li>
                        </ul>
                    </div>
                </div>



            </div>
        </div>
    </div>
    <!--====================  Conact us info End  ====================-->






    <!--========== Call to Action Area Start ============-->
    <div class="cta-image-area_one  " 
        style="background-image: url(assets/images/bg/146885430.jpg); background-size:100%; padding: 170px;">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-xl-6 col-lg-6">

                </div>
                <div class="col-xl-6 col-lg-6">
                    <div class="cta-content md-text-center">
                        <h3 class="heading text-white">Cloud : un retour sur investissement garanti.</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--========== Call to Action Area End ============-->





</div>

@endsection
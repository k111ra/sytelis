@extends('layouts.layout')



@section('containt')

<div class="site-wrapper-reveal">
    <div class="about-banner-wrap banner-space"
        style="background-image: url(assets/images/bg/148181211_xl.jpg); background-size: 100% 100%;">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 ml-auto mr-auto">
                    <div class="about-banner-content text-center">
                        <h1 class="mb-15 text-white">Partenaires</h1>
                        <h5 class="font-weight--normal text-white">Nos partenaires sont des professionnels de l'IT dont leur expertise n’est plus à démontrer localement et dans le monde.</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--===========  feature-large-images-wrapper  Start =============-->

    <!--===========  feature-large-images-wrapper  End =============-->



    <!--===========  feature-icon-wrapper  Start =============-->
    <div class="feature-icon-wrapper section-space--pb_70">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title-wrap text-center section-space--mb_20">
                        <h3 class="heading">Nos Partenaires</h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="feature-list__one">
                        <div class="row">
                            <div class="" style="width: 230px; height: 320px; background-color: #f3f3f3;              border-bottom: solid 9px #c01a04; float: left;
                            margin: 20px 13px 0 0; text-align: center; padding: 10px;">
                                <p><img class="alignnone size-full wp-image-2432"
                                        src="assets/images/partners/Veone.PNG"
                                        alt="logo-hp" width="200" height="148"></p>
                                <h3>Veone</h3>
                                <p><a href="#">Site web français</a><br>
                                    <a href="#">English website</a></p>
                            </div>
                            <div class="" style="width: 230px; height: 320px; background-color: #f3f3f3;     border-bottom: solid 9px #c01a04; float: left;
                            margin: 20px 13px 0 0; text-align: center; padding: 10px;">
                                <p><img class="alignnone size-full wp-image-2432"
                                        src="assets/images/partners/O365.png"
                                        alt="IONOS - Official Partner" width="200" height="148"></p>
                                <h3>365</h3>
                                <p><a href="#">Site web français</a><br>
                                    <a href="#">English website</a></p>
                            </div>
                            <div class="" style="width: 230px; height: 320px; background-color: #f3f3f3;     border-bottom: solid 9px #c01a04; float: left;
                            margin: 20px 13px 0 0; text-align: center; padding: 10px;">
                                <p><img class="alignnone size-full wp-image-2432"
                                        src="assets/images/partners/Just.JPG"
                                        alt="IONOS - Official Partner" width="200" height="148"></p>
                                <h3>Just</h3>
                                <p><a href="#">Site web français</a><br>
                                    <a href="#">English website</a></p>
                            </div>
                            <div class="" style="width: 230px; height: 320px; background-color: #f3f3f3;     border-bottom: solid 9px #c01a04; float: left;
                            margin: 20px 13px 0 0; text-align: center; padding: 10px;">
                                <p><img class="alignnone size-full wp-image-2432"
                                        src="assets/images/partners/Panda_Security.png"
                                        alt="Panda Security" width="200" height="148"></p>
                                <h5>Panda Security</h5> <br>
                                <p><a href="#">Site web français</a><br>
                                    <a href="#">English website</a></p>
                            </div>
                            <div class="" style="width: 230px; height: 320px; background-color: #f3f3f3;     border-bottom: solid 9px #c01a04; float: left;
                            margin: 20px 13px 0 0; text-align: center; padding: 10px;">
                                <p><img class="alignnone size-full wp-image-2432"
                                        src="assets/images/partners/Dolicloud.png"
                                        alt="Dolicloud" width="200" height="148"></p>
                                <h3>Dolicloud</h3>
                                <p><a href="#">Site web français</a><br>
                                    <a href="#">English website</a></p>
                            </div>
                            <div class="" style="width: 230px; height: 320px; background-color: #f3f3f3;     border-bottom: solid 9px #c01a04; float: left;
                            margin: 20px 13px 0 0; text-align: center; padding: 10px;">
                                <p><img class="alignnone size-full wp-image-2432"
                                        src="assets/images/partners/Adobe.png"
                                        alt="Adobe" width="200" height="148"></p>
                                <h3>Adobe</h3>
                                <p><a href="#">Site web français</a><br>
                                    <a href="#">English website</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--===========  feature-icon-wrapper  End =============-->



    <!--========== Call to Action Area Start ============-->
    <div class="cta-image-area_one section-space--ptb_80  "
        style="background-image: url(../assets/images/bg/50879264_l.jpg) ; background-size:100%;  ">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-xl-6 col-lg-5">
                    <!-- <div class="cta-button-group--one text-center" style="margin-bottom: 100px">
                        <a href="#" class="btn btn--white btn-one"><span class="btn-icon mr-2"><i
                                    class="far fa-comment-alt-dots"></i></span> Parlons en</a>
                        <a href="#" class="ht-btn ht-btn-md"><span class="btn-icon mr-2"><i
                                    class="far fa-info-circle"></i></span>Plus d'infos</a>
                    </div> -->
                </div>
                <div class="col-xl-5 col-lg-5" style="margin: 100px 0 100px 0 ;">
                    <h4>Un parfait équilibre de partenariat : choix et valeur ajoutée de la solution.</h4>
                </div>
            </div>
        </div>
    </div>
    <!--========== Call to Action Area End ============-->

</div>

@endsection
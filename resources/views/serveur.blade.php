@extends('layouts.layout')



@section('containt')

<div class="site-wrapper-reveal">
    <div class="about-banner-wrap banner-space" style="background-image: url(assets/images/bg/111288911_m.jpg); background-size: 100%;" >
        <div class="container">
            <div class="row">
                <div class="col-lg-8 ml-auto mr-auto">
                    <div class="about-banner-content text-center">
                        <h1 class="mb-15 text-white">Serveur</h1>
                        <h5 class="font-weight--normal text-white">Stocker et Sauvegarder en toute sécurité vos précieuses données et informations.</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--===========  feature-large-images-wrapper  Start =============-->
    
    <!--===========  feature-large-images-wrapper  End =============-->

   

    <!--===========  feature-icon-wrapper  Start =============-->
    <div class="feature-icon-wrapper section-space--pb_70">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title-wrap text-center section-space--mb_20">
                        <h3 class="heading">Nos offres</h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="feature-list__three">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="grid-item animate">
                                    <div class="ht-box-icon style-03">
                                       <a href="#">
                                           <div class="icon-box-wrap">
                                        <div class="content-header">
                                            <div class="icon">
                                                <i class="fal fa-download" style="color: red;"></i>
                                            </div>
                                            <h5 class="heading">
                                                Serveurs dédiés 
                                            </h5>
                                        </div>
                                        <div class="content">
                                            <div class="text">Accéder à votre propre serveur et prêt à être utilisé tout de suite : héberger votre site, votre base de données, délivrer vos services web, stocker et sauvegarder, mutualiser vos applications,..</div>
                                        </div>
                                    </div></a> 
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="grid-item animate">
                                    <div class="ht-box-icon style-03">
                                        <div class="icon-box-wrap">
                                            <div class="content-header">
                                                <div class="icon">
                                                    <i class="fal fa-inbox" style="color: red;"></i>
                                                </div>
                                                <h5 class="heading">
                                                    Serveur VPS 
                                                </h5>
                                            </div>
                                            <div class="content">
                                                <div class="text">Puissance. Disponibilité. Suivi. Déployer vos environnements de production et vos applications d'entreprise. Idéal pour du bureau à distance d'entreprise, des boutiques en ligne ou des applications critiques.</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="grid-item animate">
                                    <div class="ht-box-icon style-03">
                                        <div class="icon-box-wrap">
                                            <div class="content-header">
                                                <div class="icon">
                                                    <i class="fal fa-box-open" style="color: red;"></i>
                                                </div>
                                                <h5 class="heading">
                                                    Stockage 
                                                </h5>
                                            </div>
                                            <div class="content">
                                                <div class="text">En lieu et place de clés, de disques,etc...accéder à nos packs de stockage hautement sécurisé (SHS) pour une protection optimale de vos données. Plus de perte des données. Sécuriser, synchroniser et partager vos données.</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="grid-item animate">
                                    <div class="ht-box-icon style-03">
                                        <div class="icon-box-wrap">
                                            <div class="content-header">
                                                <div class="icon">
                                                    <i class="fal fa-box-check" style="color: red;"></i>
                                                </div>
                                                <h5 class="heading">
                                                    Sauvegarde
                                                </h5>
                                            </div>
                                            <div class="content">
                                                <div class="text">Assurer la protection de vos données et la continuité de votre activité avec notre solution de sauvegarde. Elle est dédiée aux entreprises et garanti l'intégralité de vos précieuses informations.</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--===========  feature-icon-wrapper  End =============-->


     
    <!--========== Call to Action Area Start ============-->
    <div class="cta-image-area_one section-space--ptb_80  " style="background-image: url(../assets/images/bg/127252895_m.jpg) ; background-size:100%;  " >
        <div class="container">
            <div class="row align-items-center">
                <div class="col-xl-6 col-lg-5">
                    <div class="cta-button-group--one text-center" style="margin-bottom: 100px">
                        <a href="#" class="btn btn--white btn-one"><span class="btn-icon mr-2"><i
                                    class="far fa-comment-alt-dots"></i></span> Parlons en</a>
                        <a href="#" class="ht-btn ht-btn-md"><span class="btn-icon mr-2"><i
                                    class="far fa-info-circle"></i></span>Plus d'infos</a>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6" style="margin-top: 200px; ">
                    
                </div>
            </div>
        </div>
    </div>
    <!--========== Call to Action Area End ============-->

</div>

@endsection
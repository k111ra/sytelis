<div class="footer-area-wrapper bg-gray">
    <div class="footer-area section-space--ptb_80">
        <div class="container">
            <div class="row footer-widget-wrapper">
                <div class="col-lg-4 col-md-6 col-sm-6 footer-widget">
                    <div class="footer-widget__logo mb-30">
                        <img src="assets/images/logo/SYTELIS_0.png" class="img-fluid" alt="">
                    </div>
                    <ul class="footer-widget__list">
                        <li>Riviera Palmeraie, Cocody, Abidjan, Côte-d'Ivoire</li>
                        <li><a href="mailto:contact@aeroland.com" class="hover-style-link">courriel@sytelis.ci</a>
                        </li>
                        <li><a href="tel:123344556" class="hover-style-link text-black font-weight--bold">00225 77 09 68 33</a></li>

                    </ul>
                </div>
                <div class="col-lg-2 col-md-4 col-sm-6 footer-widget">
                    <h6 class="footer-widget__title mb-20">Nos Solutions</h6>
                    <ul class="footer-widget__list">
                        <li><a href="/serveur" class="hover-style-link">Serveur</a></li>
                        <li><a href="/hebergement" class="hover-style-link">Hébergement</a></li>
                        <li><a href="#" class="hover-style-link">Sécurité</a></li>
                        <li><a href="#" class="hover-style-link">SMS de masse
                        </a></li>
                        <li><a href="#" class="hover-style-link">ERP/CRM
                        </a></li>
                        <li><a href="#" class="hover-style-link">Audit
                        </a></li>
                    </ul>
                </div>
                <div class="col-lg-2 col-md-4 col-sm-6 footer-widget">
                    <h6 class="footer-widget__title mb-20">Accès rapide</h6>
                    <ul class="footer-widget__list">
                        <li><a href="/packalpha" class="hover-style-link">Packs ALPHA</a></li>
                        <li><a href="#" class="hover-style-link">simMarket</a></li>
                        <li><a href="#" class="hover-style-link">Dolicloud</a></li>
                        <li><a href="#" class="hover-style-link">Alfresco</a></li>
                        <li><a href="#" class="hover-style-link">SHS</a></li>

                    </ul>
                </div>
                <div class="col-lg-2 col-md-4 col-sm-6 footer-widget">
                    <h6 class="footer-widget__title mb-20">Support</h6>
                    <ul class="footer-widget__list">
                        <li><a href="#" class="hover-style-link">Forum</a></li>
                        <li><a href="#" class="hover-style-link">Aide & Support</a></li>
                        <li><a href="#" class="hover-style-link">Contacter nous</a></li>
                        <li><a href="#" class="hover-style-link">Tarifs & Plans</a></li>
                    </ul>
                </div>

            </div>
        </div>
    </div>
    <div class="footer-copyright-area section-space--pb_30">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-6 text-center text-md-left">
                    <span class="copyright-text">&copy; 2021 Sytelis. <a href="#">Tous Droits Reservés.</a></span>
                </div>
                <div class="col-md-6 text-center text-md-right">
                    <ul class="list ht-social-networks solid-rounded-icon">

                        <li class="item">
                            <a href="https://twitter.com" target="_blank" aria-label="Twitter"
                                class="social-link hint--bounce hint--top hint--primary">
                                <i class="fab fa-twitter link-icon"></i>
                            </a>
                        </li>
                        <li class="item">
                            <a href="https://facebook.com" target="_blank" aria-label="Facebook"
                                class="social-link hint--bounce hint--top hint--primary">
                                <i class="fab fa-facebook-f link-icon"></i>
                            </a>
                        </li>
                        <li class="item">
                            <a href="https://instagram.com" target="_blank" aria-label="Instagram"
                                class="social-link hint--bounce hint--top hint--primary">
                                <i class="fab fa-instagram link-icon"></i>
                            </a>
                        </li>
                        <li class="item">
                            <a href="https://linkedin.com" target="_blank" aria-label="Linkedin"
                                class="social-link hint--bounce hint--top hint--primary">
                                <i class="fab fa-linkedin link-icon"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

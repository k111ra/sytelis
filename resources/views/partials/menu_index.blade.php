<!--====================  header area ====================-->
<div class="header-area header-area--absolute">

    <div class="header-top-bar-info border-bottom d-none d-lg-block">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="top-bar-wrap">
                        <div class="top-bar-left">
                            <div class="top-bar-text text-white"><a href="/packalpha"
                                    class="font-medium display-inline">News : </a> Nouveaux Packs d'hébergement
                                disponibles dès maintenant !</div>
                        </div>
                        <div class="top-bar-right">
                            <ul class="top-bar-info">
                                <li class="info-item">
                                    <a href="tel:0022577096833" class="info-link text-white">
                                        <i class="info-icon fa fa-phone"></i>
                                        <span class="info-text"><strong>00225 07 77 09 68 33</strong></span>
                                    </a>
                                </li>
                                <li class="info-item text-white">
                                    <i class="info-icon fa fa-map-marker-alt"></i>
                                    <span class="info-text">Riviera Palmeraie, Cocody, Abidjan, Côte-d'Ivoire</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header-bottom-wrap header-sticky">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="header position-relative">
                        <!-- brand logo -->
                        <div class="header__logo">
                            <a href="/">
                                <img src="assets/images/logo/LOGO SYTELIS_version_blanc.png"
                                    class="img-fluid light-logo" alt="">
                                <img src="assets/images/logo/SYTELIS_0.png" class="img-fluid dark-logo" alt="">
                            </a>
                        </div>

                        <div class="header-right">
                            <!-- navigation menu -->
                            <div class="header__navigation menu-style-four d-none d-xl-block">
                                <nav class="navigation-menu">
                                    <ul>
                                        <li class="">
                                            <a href="/"><span>ACCUEIL</span></a>


                                        </li>
                                        <li class="has-children has-children--multilevel-submenu">
                                            <a href="#"><span>SYTELIS</span></a>
                                            <ul class="submenu">
                                                <li><a href="/presentation"><span>PRESENTATION</span></a></li>
                                                <li><a href="/contact"><span>CONTACT</span></a></li>
                                                <li><a href="/partenaire"><span>PARTENAIRES</span></a></li>
                                            </ul>


                                        </li>
                                        <li class="has-children has-children--multilevel-submenu">
                                            <a href="#"><span>HÉBERGEMENT</span></a>
                                            <ul class="submenu">
                                                <li><a href="#"><span>MAILS PROFESSIONNELS & MESSAGERIE</span></a></li>
                                                <li><a href="#"><span>DOMAINE & DNS</span></a></li>
                                                <li><a href="#"><span>SITE INTERNET</span></a></li>
                                                <li><a href="#"><span>SECURITE SSL</span></a></li>

                                            </ul>

                                        </li>
                                        <li class="has-children has-children--multilevel-submenu">
                                            <a href="#"><span>CYBERSECURITE</span></a>
                                            <ul class="submenu">
                                                <li>
                                                    <a href="#">PROTECTION CONTRE LES VIRUS</a>
                                                    <ul class="submenu">
                                                        <li><a href="#"><span class="text-uppercase">Protection anti-malware managée</span></a></li>
                                                        <li><a href="#"><span class="text-uppercase">Protection contre les ransomwares et restauration</span></a></li>
                                                        <li><a href="#"><span class="text-uppercase">Gestion des solutions de sécurité Windows</span></a></li>
                                                    </ul>
                                                </li>
                                                <li><a href="#"><span class="text-uppercase">Protection Mail</span></a></li>
                                                <li><a href="#"><span class="text-uppercase">Évaluations des vulnérabilités</span></a></li>
                                                <li><a href="#"><span class="text-uppercase">Gestion des correctifs</span></a></li>

                                                <li><a href="#"><span class="text-uppercase">Filtrage et classification des URL</span></a></li>
                                                <li><a href="#"><span class="text-uppercase">POC</span></a></li>
                                                <li><a href="#"><span class="text-uppercase">Collecte de données d'investigation numérique</span></a></li>


                                            </ul>

                                        </li>
                                        <li class="has-children has-children--multilevel-submenu">
                                            <a href="#"><span> CLOUD</span></a>
                                            <!-- mega menu -->
                                            <ul class="submenu">
                                                <li><a href="#"><span>AUDIT</span></a></li>
                                                <li><a href="#"><span>ERP/CRM</span></a></li>
                                                <li><a href="#"><span>EXTERNALISATION</span></a></li>
                                                <li><a href="#"><span>GED</span></a></li>
                                                <li><a href="/hebergement"><span>HÉBERGEMENT</span></a></li>
                                                <li><a href="#"><span>SÉCURITÉ</span></a></li>
                                                <li><a href="/serveur"><span>SERVEUR</span></a></li>
                                                <li><a href="#"><span>SMS DE MASSE</span></a></li>

                                            </ul>

                                        </li>
                                        <li class="">
                                            <a href="#"><span>DOCUMENTATION</span></a>
                                            <!-- multilevel submenu -->

                                        </li>

                                    </ul>
                                </nav>
                            </div>

                            <div class="header-search-form-two white-icon">
                                <form action="#" class="search-form-top-active">
                                    <div class="search-icon" id="search-overlay-trigger">
                                        <a href="javascript:void(0)">
                                            <i class="fa fa-search"></i>
                                        </a>
                                    </div>
                                </form>
                            </div>

                            <!-- mobile menu -->
                            <div class="mobile-navigation-icon white-md-icon d-block d-xl-none"
                                id="mobile-menu-trigger">
                                <i></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<!--====================  End of header area  ====================-->

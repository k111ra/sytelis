@extends('layouts.layout')

@section('containt')

   <!-- breadcrumb-area start -->
   <div class="breadcrumb-area" style="background-image: url(assets/images/bg/106840798_s.jpg); width:100%;">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumb_box text-center">
                    <h1 class="breadcrumb-title text-color-primary">Packs ALPHA</h1>
                    <!-- breadcrumb-list start -->
                    <ul class="breadcrumb-list">
                        <li class="breadcrumb-item"><a href="/">Accueil</a></li>
                       
                        <li class="breadcrumb-item active">Packs ALPHA</li>
                    </ul>
                    <!-- breadcrumb-list end -->
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb-area end -->




<div class="site-wrapper-reveal">

    <!--========= Modal ==========-->
    <!-- Button trigger modal -->

  
  <!-- Modal -->
  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Commencez avec un pack</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            Orange Money
            00225 77 09 68 33
            Une fois le paiement effectué, veuillez nous appeler ou envoyer un SMS au même numéro ou encore un mail à commercial@sytelis.ci Merci.
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
        </div>
      </div>
    </div>
  </div>
    <!--========= Modal ==========-->

   

    <!--========= Pricing Table Area Start ==========-->
     <div class="pricing-table-area section-space--ptb_100 ">
        <div class="pricing-table-title-area position-relative">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="section-title-wrapper text-center section-space--mb_60 wow move-up">
                            <h6 class="section-sub-title mb-20">Nos offres</h6>
                            <h3 class="section-title">Des packs aux prix étudiés <span class="text-color-primary"> pour vous !</span> </h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="pricing-table-content-area">
            <div class="container">
                <div class="row pricing-table-two">
                    <div class="col-12 col-md-6 col-lg-6 col-xl-3 pricing-table wow move-up">
                        <div class="pricing-table__inner">
                            <div class="pricing-table__header">
                                <h5 class="pricing-table__title">ALPHA 1</h5>
                                <div class="pricing-table__price-wrap">
                                    
                                    <h6 class="price">40000</h6>
                                    <h6 class="currency">FCFA</h6>
                                    <h6 class="period">TTC/AN</h6>
                                </div>
                            </div>
                            <div class="pricing-table__body">
                                <ul class="pricing-table__list">
                                    <li>Domaine (.com, .ci, .org): 1 </li>
                                    <li>Messagerie : Thunderbird </li>
                                    <li>Webmail</li>
                                    <li>Espace Disque : 100Go</li>
                                    <li>Boites Mails:1</li>
                                    <li>Taille Boite Mail:30Go</li>
                                    <li>Taille de Pièces-jointes: illimité</li>
                                    <li>Signature automatique personnalité</li>
                                    <li>Sites hebergés: 1</li>
                                    <li>Sous Domaine:5</li>
                                    <li>Accès FTP:1</li>
                                    <li>Base de donnée Mysql:1</li>
                                    <li>Anti-Spam et Anti-Virus</li>
                                    <li>Bande passante : illimitée</li>
                                    <li>Boites Mails supplémentaire**</li>
                                    <li>SAV + Support 24/7</li>    
                                    <li>Messagerie : eM client(option)*</li>
                                    <li>Messagerie : Outlook Office 365 (option)*</li>
                                    
                                </ul>
                            </div>
                            <div class="pricing-table__footer">
                                <a href="#" class="ht-btn ht-btn-default btn--secondary" data-toggle="modal" data-target="#exampleModal">Commencer</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-6 col-xl-3 pricing-table pricing-table--popular wow move-up">
                        <div class="pricing-table__inner">
                            <div class="pricing-table__feature-mark">
                                <span>Le plus commandé</span>
                            </div>
                            <div class="pricing-table__header">
                                <h5 class="pricing-table__title">ALPHA 5</h5>
                                <div class="pricing-table__price-wrap">
                                    
                                    <h6 class="price">60000</h6>
                                    <h6 class="currency">FCFA</h6>
                                    <h6 class="period">TTC/AN</h6>
                                </div>
                            </div>
                            <div class="pricing-table__body">
                                <ul class="pricing-table__list">
                                    <li>Domaine (.com, .ci, .org): 1 </li> 
                                    <li>Messagerie : Thunderbird </li>
                                    <li>Webmail</li>
                                    <li>Espace Disque : 150Go</li>
                                    <li>Boites Mails:5</li>
                                    <li>Taille Boite Mail: 10Go</li>
                                    <li>Sites hebergés: 5</li>
                                    <li>Sous Domaine: illimité</li>
                                    <li>Accès FTP: illimité</li>
                                    <li>Base de donnée Mysql: illimité</li>
                                    <li>Bande passante: illimitée</li>
                                    <li>Boites Mails : supplémentaire**</li>
                                    <li>SAV + Support 24/7</li>    
                                    <li>Messagerie : eM client(option)*</li>
                                    <li>Messagerie : Outlook Office 365 (option)*</li>
                                    
                                    <br><br><br>
                                    <li style="font-size: 10px"> *Appeler +225 77 09 68 33</li>                          
                                    <li style="font-size: 10px"> **1770 FCFA TTC/MOIS</li>
                                    
                                </ul>
                            </div>
                            <div class="pricing-table__footer">
                                <a href="#" class="ht-btn ht-btn-default btn--secondary" data-toggle="modal" data-target="#exampleModal">Commencer</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-6 col-xl-3 pricing-table wow move-up">
                        <div class="pricing-table__inner">
                            <div class="pricing-table__header">
                                <h5 class="pricing-table__title">ALPHA 10</h5>
                                <div class="pricing-table__price-wrap">
                                    
                                    <h6 class="price">85000</h6>
                                    <h6 class="currency">FCFA</h6>
                                    <h6 class="period">TTC/AN</h6>
                                </div>
                            </div>
                            <div class="pricing-table__body">
                                <ul class="pricing-table__list">
                                    <li>Domaine (.com, .ci, .org): 1 </li>
                                    <li>Messagerie : Thunderbird </li>
                                    <li>Webmail</li>
                                    <li>Espace Disque : illimité</li>
                                    <li>Boites Mails: 10</li>
                                    <li>Taille Boite Mail: illimité</li>
                                    <li>Sites hebergés: 10</li>
                                    <li>Sous Domaine: illimité</li>
                                    <li>Accès FTP: illimité</li>
                                    <li>Base de donnée Mysql: illimité</li>
                                    <li>Bande passante: illimité</li>
                                    <li>Boites Mails : supplémentaire**</li>
                                    <li>SAV + Support 24/7</li>    
                                    <li>Messagerie : eM client(option)*</li>
                                    <li>Messagerie : Outlook Office 365 (option)*</li>
                                    
                                    <br><br><br>
                                    <li style="font-size: 10px"> *Appeler +225 77 09 68 33</li>                          
                                    <li style="font-size: 10px"> **1770 FCFA TTC/MOIS</li>
                                </ul>
                            </div>
                            <div class="pricing-table__footer">
                                <a href="#" class="ht-btn ht-btn-default btn--secondary" data-toggle="modal" data-target="#exampleModal">Commencer</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-6 col-xl-3 pricing-table wow move-up">
                        <div class="pricing-table__inner">
                            <div class="pricing-table__header">
                                <h5 class="pricing-table__title">ALPHA 25</h5>
                                <div class="pricing-table__price-wrap">
                                    
                                    <h6 class="price">100000</h6>
                                    <h6 class="currency">FCFA </h6>
                                    <h6 class="period">TTC/AN</h6>
                                </div>
                            </div>
                            <div class="pricing-table__body">
                                <ul class="pricing-table__list">
                                    <li>Domaine (.com, .ci, .org): 1 </li>
                                    <li>Messagerie : Thunderbird </li>
                                    <li>Webmail</li>
                                    <li>Espace Disque : illimité</li>
                                    <li>Boites Mails: 25</li>
                                    <li>Taille Boite Mail: illimité</li>
                                    <li>Sites hebergés: 10</li>
                                    <li>Sous Domaines: illimité</li>
                                    <li>Accès FTP: illimité</li>
                                    <li>Bases de données Mysql: illimitée</li>
                                    <li>Bande passante: illimitée</li>
                                    <li>Messagerie : supplémentaire**</li>
                                    <li>SAV + Support 24/7</li>    
                                    <li>Messagerie : eM client(option)*</li>
                                    <li>Messagerie : Outlook Office 365 (option)*</li>
                                    <br><br>    
                                    <li style="font-size: 10px"> *Appeler +225 77 09 68 33</li>                          
                                    <li style="font-size: 10px"> **1770 FCFA TTC/MOIS</li>                                    
                                </ul>
                            </div>
                            <div class="pricing-table__footer">
                                <a href="#" class="ht-btn ht-btn-default btn--secondary" data-toggle="modal" data-target="#exampleModal">Commencer</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--========= Pricing Table Area End ==========-->



    <!--========== Call to Action Area Start ============-->
    <div class="cta-image-area_one section-space--ptb_80  " style="background-image: url(../assets/images/bg/image-35544799.jpg) ; background-size:100%;" >
        <div class="container">
            <div class="row align-items-center">
                <div class="col-xl-6 col-lg-5">
                    <div class="cta-content md-text-center">
                        <h3 class="heading text-white">N'hésitez pas à nous contacter....</h3>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6" style="margin-top: 200px; ">
                    <div class="cta-button-group--one text-center">
                        <a href="#" class="btn btn--white btn-one"><span class="btn-icon mr-2"><i
                                    class="far fa-comment-alt-dots"></i></span>00225 77 09 68 33</a>
                        <a href="#" class="ht-btn ht-btn-md"><span class="btn-icon mr-2"><i
                                    class="far fa-info-circle"></i></span> commercial@sytelis.ci</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--========== Call to Action Area End ============-->









</div>


@endsection
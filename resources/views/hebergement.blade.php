@extends('layouts.layout')



@section('containt')

<div class="site-wrapper-reveal">
    <div class="about-banner-wrap banner-space bg-img" data-bg="assets/images/bg/33548890_l.jpg" >
        <div class="container">
            <div class="row">
                <div class="col-lg-8 ml-auto mr-auto">
                    <div class="about-banner-content text-center">
                        <h1 class="mb-15 text-white">Hébergement</h1>
                        <h5 class="font-weight--normal text-white">Hébergez votre site web et vos emails professionnels en toute tranquillité.</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--===========  feature-large-images-wrapper  Start =============-->
    {{-- <div class="feature-large-images-wrapper section-space--ptb_100">
        <div class="container">
            <div class="cybersecurity-about-box">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="modern-number-01">
                            <h3 class="heading mt-30">Learn More About Our <span class="text-color-primary"> Success
                                    <br> Stories</span></h3>
                        </div>
                    </div>
                    <div class="col-lg-7 offset-lg-1">
                        <div class="conact-us-wrap-one managed-it">
                            <h5 class="heading ">Mitech specializes in <span class="text-color-primary">
                                    technological and IT-related services</span> such as product engineering,
                                warranty management, building cloud, infrastructure, network, etc. </h5>

                            <div class="sub-heading">We’re available for 8 hours a day!<br>Contact to require a
                                detailed analysis and assessment of your plan.</div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div> --}}
    <!--===========  feature-large-images-wrapper  End =============-->

    {{-- <div class="accordion-wrapper section-space--pb_100">
        <div class="container">
            <div class="row ">
                <div class="col-lg-6 pr-5">
                    <div class="faq-two-wrapper section-space--mt_40">
                        <div id="accordion_two">
                            <div class="card">
                                <div class="card-header" id="heading__10">
                                    <h5 class="mb-0 font-weight--bold">
                                        <button class="btn-link" data-toggle="collapse" data-target="#tab__10"
                                            aria-expanded="true" aria-controls="tab__10">
                                            New multiple site connectivity <span>
                                                <i class="far fa-caret-circle-down"></i>
                                                <i class="far fa-caret-circle-right"></i> </span>
                                        </button>
                                    </h5>
                                </div>

                                <div id="tab__10" class="show" aria-labelledby="heading__10"
                                    data-parent="#accordion_two">
                                    <div class="card-body">
                                        <p>We use a newly developed technology to connect sites that are based on
                                            different types of servers and networks, SiteConnect, which helps to
                                            reduce the misinterpretation of signals as well as the loss of data
                                            during transfering. </p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="heading__11">
                                    <h5 class="mb-0">
                                        <button class="btn-link collapsed" data-toggle="collapse"
                                            data-target="#collapse__11" aria-expanded="false"
                                            aria-controls="collapse__11">
                                            IT Security & Software<span>
                                                <i class="far fa-caret-circle-down"></i>
                                                <i class="far fa-caret-circle-right"></i> </span>
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapse__11" class="collapse" aria-labelledby="heading__11"
                                    data-parent="#accordion_two">
                                    <div class="card-body">
                                        <p>To keep your systems, your devices, and network stay secure, we have
                                            developed a new program that limits the access of suspicious objects or
                                            people and authenticate all logins to the system. </p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="heading__12">
                                    <h5 class="mb-0">
                                        <button class="btn-link collapsed" data-toggle="collapse"
                                            data-target="#collapse__12" aria-expanded="false"
                                            aria-controls="collapse__12">
                                            Weak hosted capability<span><i class="far fa-caret-circle-down"></i>
                                                <i class="far fa-caret-circle-right"></i> </span>
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapse__12" class="collapse" aria-labelledby="heading__12"
                                    data-parent="#accordion_two">
                                    <div class="card-body">
                                        <p>Some hosts are unaware of the potential risks as well as security
                                            loopholes in their system. By detecting these errors and taking prompt
                                            actions on improving firewalls, we can upgrade the system security. </p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="heading__13">
                                    <h5 class="mb-0">
                                        <button class="collapsed" data-toggle="collapse" data-target="#collapse__13"
                                            aria-expanded="false" aria-controls="collapse__13">
                                            Build internal network <span><i class="far fa-caret-circle-down"></i>
                                                <i class="far fa-caret-circle-right"></i> </span>
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapse__13" class="collapse" aria-labelledby="heading__13"
                                    data-parent="#accordion_two">
                                    <div class="card-body">
                                        <p>The internal network is essential for all companies and corporations,
                                            especially for those working in IT sector. To avoid possible risks when
                                            sharing internal confidential files and documentation to an external
                                            receiver, internal network must be strong. </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6">
                    <!-- Start single-popup-wrap -->
                    <div class="video-interview section-space--mt_40 video-popup">
                        <a href="https://www.youtube.com/watch?v=9No-FiEInLA" class="video-link mt-30">
                            <div class="single-popup-wrap">
                                <img class="img-fluid border-radus-5"
                                    src="assets/images/bg/mitech-home-infotechno-box-large-image-03-540x320.jpg"
                                    alt="">
                                <div class="ht-popup-video video-button">
                                    <div class="video-mark">
                                        <div class="wave-pulse wave-pulse-1"></div>
                                        <div class="wave-pulse wave-pulse-2"></div>
                                    </div>
                                    <div class="video-button__two">
                                        <div class="video-play">
                                            <span class="video-play-icon"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <!-- End single-popup-wrap -->
                </div>
            </div>
        </div>
    </div> --}}

    <!--===========  feature-icon-wrapper  Start =============-->
    <div class="feature-icon-wrapper section-space--pb_70">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title-wrap text-center section-space--mb_20">
                        <h3 class="heading">Nos offres</h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="feature-list__three">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="grid-item animate">
                                    <div class="ht-box-icon style-03">
                                       <a href="/packalpha">
                                           <div class="icon-box-wrap">
                                        <div class="content-header">
                                            <div class="icon">
                                                <i class="fal fa-server" style="color: red;"></i>
                                            </div>
                                            <h5 class="heading">
                                                Messagerie professionnelle 
                                            </h5>
                                        </div>
                                        <div class="content">
                                            <div class="text">Les packs ALPHA vous servent sur un même plateau un domaine, des adresses mails professionnels, de l'espace disque, etc...pour très petite, moyenne ou grande entreprise... et même pour solo-entrepreneur. Qu'attendez-vous pour vous servir ?</div>
                                        </div>
                                    </div></a> 
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="grid-item animate">
                                    <div class="ht-box-icon style-03">
                                        <div class="icon-box-wrap">
                                            <div class="content-header">
                                                <div class="icon">
                                                    <img src="assets/images/icons/www.png" width="50px" alt="">
                                                </div>
                                                <h5 class="heading">
                                                    Domaines 
                                                </h5>
                                            </div>
                                            <div class="content">
                                                <div class="text">Grâce à nos offres de domaines, identifiez vous sur le net et restez unique auprès de tous vos partenaires ! Des extensions locales et internationales vous attendent afin de vous singularisez face à la concurrence. Qu'attendez-vous pour vous en procurez ?</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="grid-item animate">
                                    <div class="ht-box-icon style-03">
                                        <div class="icon-box-wrap">
                                            <div class="content-header">
                                                <div class="icon">
                                                    <i class="fal fa-browser" style="color: red;"></i>
                                                </div>
                                                <h5 class="heading">
                                                    Site internet 
                                                </h5>
                                            </div>
                                            <div class="content">
                                                <div class="text">Parce que nous savons toute l'importance que vous accordez à votre site, nous apportons un soin plus que particulier à sa conception en le faisant avec vous.  Ainsi, ensemble nous donnons vie à vos projets et à vos passions. Qu'attendez vous pour en disposer ?</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="grid-item animate">
                                    <div class="ht-box-icon style-03">
                                        <div class="icon-box-wrap">
                                            <div class="content-header">
                                                <div class="icon">
                                                    <i class="fal fa-at" style="color: red;"></i>
                                                </div>
                                                <h5 class="heading">
                                                    Mails professionnels
                                                </h5>
                                            </div>
                                            <div class="content">
                                                <div class="text">Renforcez la confiance de vos clients en octroyant à chacun de vos collaborateurs une adresse e-mail professionnelle au sein de votre domaine, comme suzanne@votreentreprise.ci et jean@votreentreprise.com . Vous pouvez également créer des listes de diffusion telles que ventes@votreentreprise.net.</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--===========  feature-icon-wrapper  End =============-->


     <!--========= Pricing Table Area Start ==========-->
     {{-- <div class="pricing-table-area section-space--ptb_100 ">
        <div class="pricing-table-title-area position-relative">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="section-title-wrapper text-center section-space--mb_60 wow move-up">
                            <h6 class="section-sub-title mb-20">Nos offres</h6>
                            <h3 class="section-title">Des packs aux prix étudiés <span class="text-color-primary"> pour vous !</span> </h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="pricing-table-content-area">
            <div class="container">
                <div class="row pricing-table-two">
                    <div class="col-12 col-md-6 col-lg-6 col-xl-3 pricing-table wow move-up">
                        <div class="pricing-table__inner">
                            <div class="pricing-table__header">
                                <h5 class="pricing-table__title">ALPHA 1</h5>
                                <div class="pricing-table__price-wrap">
                                    <h6 class="currency">$</h6>
                                    <h6 class="price">0</h6>
                                    <h6 class="period">/mo</h6>
                                </div>
                            </div>
                            <div class="pricing-table__body">
                                <ul class="pricing-table__list">
                                    <li>Domaine (.com, .ci, .org): 1 </li>
                                    <li>Espace Disque : 50Go</li>
                                    <li>Compte Mail:1</li>
                                    <li>Taille Boite Mail:50Go</li>
                                    <li>Taille de Pièces-jointes: illimitée</li>
                                    <li>Signature automatique personnalité</li>
                                    <li>Site hebergés: 1</li>
                                    <li>Sous Domaine:5</li>
                                    <li>Accès FTP:1</li>
                                    <li>Base de donnée Mysql:1</li>
                                    <li>Anti-Spam et Anti-Virus</li>
                                    <li> Bande passante: illimitée</li>
                                </ul>
                            </div>
                            <div class="pricing-table__footer">
                                <a href="#" class="ht-btn ht-btn-default btn--secondary">Get started</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-6 col-xl-3 pricing-table pricing-table--popular wow move-up">
                        <div class="pricing-table__inner">
                            <div class="pricing-table__feature-mark">
                                <span>Popular Choice</span>
                            </div>
                            <div class="pricing-table__header">
                                <h5 class="pricing-table__title">ALPHA 5</h5>
                                <div class="pricing-table__price-wrap">
                                    <h6 class="currency">$</h6>
                                    <h6 class="price">19</h6>
                                    <h6 class="period">/mo</h6>
                                </div>
                            </div>
                            <div class="pricing-table__body">
                                <ul class="pricing-table__list">
                                    <li>Domaine (.com, .ci, .org): 1 </li>
                                    <li>Espace Disque : illimitée</li>
                                    <li>Compte Mail:5</li>
                                    <li>Taille Boite Mail: illimitée</li>
                                    <li>Site hebergés: 5</li>
                                    <li>Sous Domaine: illimitée</li>
                                    <li>Accès FTP: illimitée</li>
                                    <li>Base de donnée Mysql: illimitée</li>
                                    <li> Bande passante: illimitée</li>
                                    <li>Boite Additionnelle: Option</li>
                                    <br><br><br>.
                                </ul>
                            </div>
                            <div class="pricing-table__footer">
                                <a href="#" class="ht-btn ht-btn-default btn--secondary">Get started</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-6 col-xl-3 pricing-table wow move-up">
                        <div class="pricing-table__inner">
                            <div class="pricing-table__header">
                                <h5 class="pricing-table__title">ALPHA 10</h5>
                                <div class="pricing-table__price-wrap">
                                    <h6 class="currency">$</h6>
                                    <h6 class="price">29</h6>
                                    <h6 class="period">/mo</h6>
                                </div>
                            </div>
                            <div class="pricing-table__body">
                                <ul class="pricing-table__list">
                                    <li>Domaine (.com, .ci, .org): 1 </li>
                                    <li>Espace Disque : illimitée</li>
                                    <li>Compte Mail: 10</li>
                                    <li>Taille Boite Mail: illimitée</li>
                                    <li>Site hebergés: 10</li>
                                    <li>Sous Domaine: illimitée</li>
                                    <li>Accès FTP: illimitée</li>
                                    <li>Base de donnée Mysql: illimitée</li>
                                    <li> Bande passante: illimitée</li>
                                    <li>Boite Additionnelle: Option</li>
                                    <br><br><br>.
                                </ul>
                            </div>
                            <div class="pricing-table__footer">
                                <a href="#" class="ht-btn ht-btn-default btn--secondary">Get started</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-6 col-xl-3 pricing-table wow move-up">
                        <div class="pricing-table__inner">
                            <div class="pricing-table__header">
                                <h5 class="pricing-table__title">ALPHA 25</h5>
                                <div class="pricing-table__price-wrap">
                                    <h6 class="currency">$</h6>
                                    <h6 class="price">49</h6>
                                    <h6 class="period">/mo</h6>
                                </div>
                            </div>
                            <div class="pricing-table__body">
                                <ul class="pricing-table__list">
                                    <li>Domaine (.com, .ci, .org): 1 </li>
                                    <li>Espace Disque : illimitée</li>
                                    <li>Compte Mail: 25</li>
                                    <li>Taille Boite Mail: illimitée</li>
                                    <li>Site hebergés: 10</li>
                                    <li>Sous Domaine: illimitée</li>
                                    <li>Accès FTP: illimitée</li>
                                    <li>Base de donnée Mysql: illimitée</li>
                                    <li> Bande passante: illimitée</li>
                                    <li>Boite Additionnelle: Option</li>
                                    <br><br><br>.
                                </ul>
                            </div>
                            <div class="pricing-table__footer">
                                <a href="#" class="ht-btn ht-btn-default btn--secondary">Get started</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> --}}
    <!--========= Pricing Table Area End ==========-->










    <!--========== Call to Action Area Start ============-->
    <div class="cta-image-area_one section-space--ptb_80  " style="background-image: url(../assets/images/image1.png) ; background-size:100%;  " >
        <div class="container">
            <div class="row align-items-center">
                <div class="col-xl-6 col-lg-5">
                    <div class="cta-content md-text-center">
                        <h3 class="heading text-white">C'est professionnel de s'héberger chez SYTELIS.</h3>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6" style="margin-top: 200px; ">
                    <div class="cta-button-group--one text-center">
                        <a href="#" class="btn btn--white btn-one"><span class="btn-icon mr-2"><i
                                    class="far fa-comment-alt-dots"></i></span> Parlons en</a>
                        <a href="#" class="ht-btn ht-btn-md"><span class="btn-icon mr-2"><i
                                    class="far fa-info-circle"></i></span>Plus d'infos</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--========== Call to Action Area End ============-->

</div>

@endsection
@extends('layouts.layout')

@section('containt')
<div class="site-wrapper-reveal">
    <!--============ Resolutions Hero Start ============-->
    <div class="service-hero-wrapper resolutions-hero-bg position-relative">
        <div id="carousel-example-2" class="carousel  slide carousel-fade" data-ride="carousel">
            <!--Indicators-->

            <!--/.Indicators-->
            <!--Slides-->
            <div class="carousel-inner" role="listbox">
                <div class="carousel-item active">
                    <div class="view">
                        <img class="d-block w-100" src="assets/images/hero/37093472_l.jpg" alt="First slide">
                        <div class="mask rgba-black-light"></div>
                        <div class="carousel-caption " style="margin: 15% 0 15% 0;">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 ml-auto mr-auto">
                                    <div class="service-hero-wrap wow move-up">
                                        <div class="service-hero-text text-center">
                                            <h3 class="text-white">La flexibilité du</h3>
                                            <h1 class="font-weight--reguler text-white mb-20">CLOUD</h1>
                                            <p class="" style="font-size: 30px; color:rgb(230, 31, 48);">Le Cloud pour une entreprise plus intelligente.</p>

                                            <div class="hero-button-group section-space--mt_50">
                                                {{-- <a href="#" class="ht-btn ht-btn-md">Nous Consulter</a> --}}
                                                <a href="/packalpha" class="ht-btn ht-btn-md btn--white"><span class="btn-icon mr-2"><i class="fa fa-play"></i></span>  En savoir plus</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- <div class="carousel-caption">
                        <div class="container" style="padding: 25% 0 25% 0">
                            <div class="row" >
                                <div class="col-lg-12 col-md-12 ml-auto mr-auto">
                                    <div class="service-hero-wrap wow move-up">
                                        <div class="service-hero-text text-center">
                                            <h3 class="text-white">La flexibilité du</h3>
                                            <h1 class="text-white mb-30">CLOUD</h1>
                                            <p class="" style=" color:rgb(91, 235, 91);">Le Cloud pour une entreprise plus intelligente.
                                            </p>

                                            <div class="hero-button-group section-space--mt_50 ">
                                                <a href="#" class="ht-btn ht-btn-md">Nous Consulter</a>
                                                <a href="#" class="ht-btn ht-btn-md btn--white"><span
                                                        class="btn-icon mr-2"><i class="fa fa-play"></i></span> En savoir plus</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div> --}}

                </div>
                <div class="carousel-item">
                    <!--Mask color-->
                    <div class="view">
                        <img class="d-block w-100" src="assets/images/hero/49817864_l.jpg" alt="Second slide">
                        <div class="mask rgba-black-strong"></div>
                        <div class="carousel-caption " style="margin: 15% 0 15% 20%;">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 ml-auto mr-auto">
                                    <div class="service-hero-wrap wow move-up">
                                        <div class="service-hero-text text-center">
                                            <h4 class="text-white">Ayez votre </h4>
                                            <h2 class="font-weight--reguler text-white mb-20">MAIL PROFESSIONNELLE</h2>
                                            <p class="" style="font-size: 30px; color:rgb(230, 31, 48);">et démarquez vous.</p>

                                            <div class="hero-button-group section-space--mt_50">
                                                {{-- <a href="#" class="ht-btn ht-btn-md">Nous Consulter</a> --}}
                                                <a href="/packalpha" class="ht-btn ht-btn-md btn--white"><span class="btn-icon mr-2"><i class="fa fa-play"></i></span>  En savoir plus</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- <div class="carousel-caption">
                        <div class="container" style="padding: 25% 0 25% 0">
                            <div class="row justify-content-end"  >
                                <div class="col-lg-6"></div>
                                <div class="col-lg-6 ">
                                    <div class="service-hero-wrap wow move-up">
                                        <div class="service-hero-text text-center">
                                            <h3 class="text-white">Ayez votre</h3>
                                            <h1 class="font-weight--reguler text-white mb-30" style="">PROPRE </h1>
                                            <p class="" style=" color:rgb(91, 235, 91);">et démarquez vous.</p>

                                            <div class="hero-button-group section-space--mt_50">
                                                <a href="#" class="ht-btn ht-btn-md">Nous Consulter</a>
                                                <a href="/packalpha" class="ht-btn ht-btn-md btn--white"><span
                                                        class="btn-icon mr-2"><i class="fa fa-play"></i></span> En savoir plus</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> --}}

                </div>
                <div class="carousel-item">
                    <!--Mask color-->
                    <div class="view">
                        <img class="d-block w-100" src="assets/images/hero/121128514_l.jpg" alt="Third slide">
                        <div class="mask rgba-black-slight"></div>
                        <div class="carousel-caption" style="margin: 15% 0 15% 20%;">
                                <div class="row ">
                                    <div class="col-lg-12 col-md-12 ml-auto" >
                                        <div class="service-hero-wrap wow move-up">
                                            <div class="service-hero-text text-center">
                                                <h4 class="text-white">Vos données sauvegardées et</h4>
                                                <h2 class="font-weight-bold mb-20" style="color:rgb(230, 31, 48);">HAUTEMENT SECURISEES</h2>
                                                <p class="" style="font-size: 30px;"> avec nos offres SHS.</p>

                                                <div class="hero-button-group section-space--mt_50">
                                                    {{-- <a href="#" class="ht-btn ht-btn-md">Nous Consulter</a> --}}
                                                    <a href="/packalpha" class="ht-btn ht-btn-md btn--white"><span class="btn-icon mr-2"><i class="fa fa-play"></i></span>  En savoir plus</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                        </div>
                    </div>
                    {{-- <div class="carousel-caption">
                        <div class="container" style="padding: 25% 0 25% 0">
                            <div class="row justify-content-end " >
                                <div class="col-lg-6 ">
                                    <div class="service-hero-wrap wow move-up">
                                        <div class="service-hero-text text-center">
                                            <h3 class="text-white">Vos données stockées et </h3>
                                            <h1 class="" style="color:rgb(91, 235, 91);" >HAUTEMENT </h1>
                                            <p class="" style=""> avec nos offres SHS.</p>

                                            <div class="hero-button-group section-space--mt_50 ">
                                                <a href="#" class="ht-btn ht-btn-md">Nous Consulter</a>
                                                <a href="#" class="ht-btn ht-btn-md btn--white"><span
                                                        class="btn-icon mr-2"><i class="fa fa-play"></i></span> En savoir plus</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> --}}

                </div>
            </div>
            <!--/.Slides-->
            <!--Controls-->
            <a class="carousel-control-prev" href="#carousel-example-2" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carousel-example-2" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
            <!--/.Controls-->

        </div>
        <div class="vc_row-separator center_curve_alt bottom"><svg xmlns="http://www.w3.org/2000/svg" version="1.1"
                preserveAspectRatio="none" viewBox="0 0 100 100">
                <path d="M 0 0 L0 100 L100 100 L100 0 Q 50 200 0 0"></path>
            </svg></div>
    </div>
    <!--============ Resolutions Hero End ============-->

    <!--============ text-slide ============-->



    <!--============ text-slide ============-->




    <!--===========  feature-images-wrapper  Start =============-->
    <div class="feature-images-wrapper bg-gray">
        <div class="container">

            <div class="row">
                <div class="col-12">
                    <div class="feature-images__five resolutions-hero-bottom">
                        <div class="row">

                            <div class="col-lg-4 col-md-6 wow move-up">
                                <!-- ht-box-icon Start -->
                                <div class="ht-box-images style-05">
                                    <div class="image-box-wrap">
                                        <div class="box-image">
                                            <div class="default-image">
                                                <img class="img-fulid"
                                                    src="assets/images/svg/Cloud_Computing.png" width="200px"                                                    alt="" >
                                            </div>
                                            <div class="hover-images">
                                                <img class="img-fulid"
                                                src="assets/images/svg/Cloud_Computing.png" width="200px"
                                                    alt="" >
                                            </div>
                                        </div>
                                        <div class="content">
                                            <h5 class="heading">CLOUD COMPUTING</h5>
                                            <div class="text">Aujourd'hui, beaucoup ont adopté la flexibilité et l'évolutivité du cloud.  SYTELIS avec l'appui de ses partenaires stratégiques vous offre des solutions cloud computing à forte valeur ajoutée. Faites la différence avec la concurrence pour une productivité optimisée. <br>.
                                            </div>
                                            <div class="box-images-arrow">
                                                <a href="#">
                                                    <span class="button-text">Découvrir Maintenant</span>
                                                    <i class="far fa-long-arrow-right"></i>
                                                </a>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- ht-box-icon End -->
                            </div>

                            <div class="col-lg-4 col-md-6 wow move-up">
                                <!-- ht-box-icon Start -->
                                <div class="ht-box-images style-05">
                                    <div class="image-box-wrap">
                                        <div class="box-image">
                                            <div class="default-image">
                                                <img class="img-fulid"
                                                src="assets/images/svg/Hébergement.png" width="300px"
                                                    alt="">
                                            </div>
                                            <div class="hover-images">
                                                <img class="img-fulid"
                                                    src="assets/images/svg/Hébergement.png" width="300px"
                                                    alt="">
                                            </div>
                                        </div>
                                        <div class="content">
                                            <h5 class="heading">HEBERGEMENT</h5>
                                            <div class="text">Nos offres d'hébergement vous permettent  désormais de pouvoir accéder à vos adresses professionnelles (au nom de votre entreprise), de détenir votre site internet à valeur ajoutée et de le sécurisé.
                                                Nos packs d'hébergement (ALPHA) sont un excellent moyen en une fois d'être présent sur internet de manière professionnel et sécurisé.
                                            </div>
                                            <div class="box-images-arrow">
                                                <a href="#">
                                                    <span class="button-text">Découvrir Maintenant</span>
                                                    <i class="far fa-long-arrow-right"></i>
                                                </a>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- ht-box-icon End -->
                            </div>

                            <div class="col-lg-4 col-md-6 wow move-up">
                                <!-- ht-box-icon Start -->
                                <div class="ht-box-images style-05">
                                    <div class="image-box-wrap">
                                        <div class="box-image">
                                            <div class="default-image">
                                                <img class="img-fulid"
                                                src="assets/images/svg/vectoriel.jpg" width="200px"
                                                    alt="">
                                            </div>
                                            <div class="hover-images">
                                                <img class="img-fulid"                                          src="assets/images/svg/vectoriel.jpg" width="200px"
                                                    alt="">
                                            </div>
                                        </div>
                                        <div class="content">
                                            <h5 class="heading">CYBERSECURITE</h5>
                                            <div class="text">Il est clair que, dans le panorama moderne des cybermenaces, les entreprises ont besoin de solutions qui ne se limitent pas à la sauvegarde des données ou à la seule protection antivirus. SYTELIS  vous offre des solutions de cybersécurité pour mieux protéger vos données, vos finances et votre propriété intellectuelle.
                                            </div>
                                            <div class="box-images-arrow">
                                                <a href="#">
                                                    <span class="button-text">Découvrir Maintenant</span>
                                                    <i class="far fa-long-arrow-right"></i>
                                                </a>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- ht-box-icon End -->
                            </div>

                        </div>
                    </div>

                    <div class="section-under-heading text-center section-space--mt_80">Nous sommes Intégrateur de Solutions Cloud et Prestataire de Services IT.  <a href="#"> En savoir plus.</a></div>

                </div>
            </div>
        </div>
    </div>
    <!--===========  feature-images-wrapper  End =============-->





    <!--=========== fun fact Wrapper Start ==========-->

    <!--=========== fun fact Wrapper End ==========-->

    <!--===========  feature-icon-wrapper  Start =============-->
    <div class="feature-icon-wrapper section-space--ptb_100">
        <div class="container">

            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title-wrap text-center section-space--mb_40">
                        <h6 class="section-sub-title mb-20">ENTREPRISES VOICI NOS OFFRES</h6>
                        <h3 class="heading">Pour chaque type d'entreprise, nous avons <br> <span
                                class="text-color-primary">une solution  cloud sur mesure et hautement fiable.</span></h3>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <div class="feature-list__one">
                        <div class="row">
                            <div class="col-lg-3 col-md-6 wow move-up">
                                <!-- ht-box-icon Start -->
                                <div class="ht-box-icon style-01 single-svg-icon-box">
                                    <div class="icon-box-wrap">
                                        <div class="icon">
                                            <div class="svg-icon" id="svg-icon-1"
                                                data-svg-icon="assets/images/svg/server.svg"></div>
                                        </div>
                                        <div class="content">
                                            <h5 class="heading">Serveur </h5>
                                            <div class="text">Avec l'appui de nos partenaires, nous vous offrons une gamme de serveurs dédiés performants et adaptés à vos activités et aux besoins les plus exigeants.
                                            </div>
                                            <div class="feature-btn">
                                                <a href="/serveur">
                                                    <span class="button-text">Voir plus</span>
                                                    <i class="far fa-long-arrow-right"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- ht-box-icon End -->
                            </div>
                            <div class="col-lg-3 col-md-6 wow move-up">
                                <!-- ht-box-icon Start -->
                                <div class="ht-box-icon style-01 single-svg-icon-box">
                                    <div class="icon-box-wrap">
                                        <div class="icon">
                                            <div class="svg-icon" id="svg-icon-2"
                                                data-svg-icon="assets/images/svg/database.svg"></div>
                                        </div>
                                        <div class="content">
                                            <h5 class="heading">Hébergements</h5>
                                            <div class="text">Nos hébergements donnent la possibilité de vous doter de mails professionnels, de sites web et d'un nom de domaine...et plus encore. Profitez de nos packs .
                                            </div>
                                            <div class="feature-btn">
                                                <a href="/hebergement">
                                                    <span class="button-text">Voir plus</span>
                                                    <i class="far fa-long-arrow-right"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- ht-box-icon End -->
                            </div>
                            <div class="col-lg-3 col-md-6 wow move-up">
                                <!-- ht-box-icon Start -->
                                <div class="ht-box-icon style-01 single-svg-icon-box">
                                    <div class="icon-box-wrap">
                                        <div class="icon">
                                            <div class="svg-icon" id="svg-icon-3"
                                                data-svg-icon="assets/images/svg/security.svg"></div>
                                        </div>
                                        <div class="content">
                                            <h5 class="heading">Sécurité</h5>
                                            <div class="text">Nous sécurisons votre cloud car nous avons des solutions à forte valeur ajoutée. <br> Nous ne négocions pas avec votre sécurité.
                                            </div>
                                            <div class="feature-btn">
                                                <a href="#">
                                                    <span class="button-text">Voir plus</span>
                                                    <i class="far fa-long-arrow-right"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- ht-box-icon End -->
                            </div>
                            <div class="col-lg-3 col-md-6 wow move-up">
                                <!-- ht-box-icon Start -->
                                <div class="ht-box-icon style-01 single-svg-icon-box">
                                    <div class="icon-box-wrap">
                                        <div class="icon">
                                            <div class="svg-icon" id="svg-icon-4"
                                                data-svg-icon="assets/images/svg/7183314801571183075.svg"></div>
                                        </div>
                                        <div class="content">
                                            <h5 class="heading">SMS de masse</h5>
                                            <div class="text">Communiquez massivement auprès de votre audience (clients, etc...) et faites la promotion de votre activité avec les campagnes de SMS en masse.  Prenez de l'avance...
                                            </div>
                                            <div class="feature-btn">
                                                <a href="#">
                                                    <span class="button-text">Voir plus</span>
                                                    <i class="far fa-long-arrow-right"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- ht-box-icon End -->
                            </div>
                            <div class="col-lg-3 col-md-6 wow move-up">
                                <!-- ht-box-icon Start -->
                                <div class="ht-box-icon style-01 single-svg-icon-box">
                                    <div class="icon-box-wrap">
                                        <div class="icon">
                                            <div class="svg-icon" id="svg-icon-5"
                                                data-svg-icon="assets/images/svg/8927748351578463645.svg"></div>
                                        </div>
                                        <div class="content">
                                            <h5 class="heading">ERP/CRM</h5>
                                            <div class="text">Dotez vous d'un outil capable de superviser et de piloter votre entreprise de A-Z. Toutes les différentes opérations internes de l'entreprise sont pris en charge depuis une console unique.
                                            </div>
                                            <div class="feature-btn">
                                                <a href="#">
                                                    <span class="button-text">Voir plus</span>
                                                    <i class="far fa-long-arrow-right"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- ht-box-icon End -->
                            </div>
                            <div class="col-lg-3 col-md-6 wow move-up">
                                <!-- ht-box-icon Start -->
                                <div class="ht-box-icon style-01 single-svg-icon-box">
                                    <div class="icon-box-wrap">
                                        <div class="icon">
                                            <div class="svg-icon" id="svg-icon-6"
                                                data-svg-icon="assets/images/svg/6728820971582956841.svg">
                                            </div>
                                        </div>
                                        <div class="content">
                                            <h5 class="heading">GED</h5>
                                            <div class="text">1 heure par jour est le temps moyen consacré à rechercher des documents  par un employé . Nous avons enfin l'outil permettant à toute entreprise de maximiser la productivité de son équipe.
                                            </div>
                                            <div class="feature-btn">
                                                <a href="#">
                                                    <span class="button-text">Voir plus</span>
                                                    <i class="far fa-long-arrow-right"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- ht-box-icon End -->
                            </div>
                            <div class="col-lg-3 col-md-6 wow move-up">
                                <!-- ht-box-icon Start -->
                                <div class="ht-box-icon style-01 single-svg-icon-box">
                                    <div class="icon-box-wrap">
                                        <div class="icon">
                                            <div class="svg-icon" id="svg-icon-7"
                                                data-svg-icon="assets/images/svg/8012323861581424141.svg"></div>
                                        </div>
                                        <div class="content">
                                            <h5 class="heading">Externalisation</h5>
                                            <div class="text">Votre système d’information est le centre névralgique de votre entreprise, vous ne pouvez donc pas vous permettre d’en négliger sa gestion en la confiant à une personne inexpérimentée.
                                            </div>
                                            <div class="feature-btn">
                                                <a href="#">
                                                    <span class="button-text">Voir plus</span>
                                                    <i class="far fa-long-arrow-right"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- ht-box-icon End -->
                            </div>
                            <div class="col-lg-3 col-md-6 wow move-up">
                                <!-- ht-box-icon Start -->
                                <div class="ht-box-icon style-01 single-svg-icon-box">
                                    <div class="icon-box-wrap">
                                        <div class="icon">
                                            <div class="svg-icon" id="svg-icon-8"
                                                data-svg-icon="assets/images/svg/linea-basic-gear.svg"></div>
                                        </div>
                                        <div class="content">
                                            <h5 class="heading">Audit</h5>
                                            <div class="text">Nos offres d'audit IT constituent de bonnes solutions pour différentes organisations en vue de faire le point sur l’état du système, ses faiblesses, ses points d’amélioration et agir en conséquence.
                                            </div>
                                            <div class="feature-btn">
                                                <a href="#">
                                                    <span class="button-text">Voir plus</span>
                                                    <i class="far fa-long-arrow-right"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- ht-box-icon End -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--===========  feature-icon-wrapper  End =============-->
    <!--====================  Conact us Section Start ====================-->
    <div class="contact-us-section-wrappaer processing-contact-us-bg section-space--ptb_120">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 col-lg-6">
                    <div class="conact-us-wrap-one">
                        <h3 class="heading text-white">Que la force du Cloud soit avec vous !</h3>

                        <div class="sub-heading text-white">Nous sommes disponibles pour vous 8 heures par jour !<br>Contactez nous pour toute information supplémentaire ou pour un support.</div>

                    </div>
                </div>

                <div class="col-lg-6 col-lg-6">
                    <div class="contact-info-two text-center">
                        <div class="icon">
                            <span class="fal fa-phone"></span>
                        </div>
                        <h6 class="heading font-weight--reguler">Appelez nous maintenant !</h6>
                        <h2 class="call-us"><a href="tel:0022577096833" style="color: rgb(91, 235, 91)">00225 77 09 68 33</a></h2>
                        <div class="contact-us-button mt-20">
                            <a href="#" class="btn btn--secondary">
                                Contactez nous
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--====================  Conact us Section End  ====================-->
    <!--====================  testimonial section ====================-->

    <!--====================  End of testimonial section  ====================-->

    <!--===========  feature-icon-wrapper  Start =============-->
    <div class="feature-icon-wrapper section-space--ptb_80 border-bottom">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-title-wrap text-center section-space--mb_40">
                        {{-- <h6 class="section-sub-title mb-20">Testimonials</h6> --}}
                        <h3 class="heading">POURQUOI CHOISIR <span class="text-color-primary"> SYTELIS ?</span>
                        </h3>
                    </div>
                    <div class="feature-list__three">
                        <div class="row">
                            <div class="col-lg-6 wow move-up">
                                <div class="grid-item animate">
                                    <div class="ht-box-icon style-03">
                                        <div class="icon-box-wrap">
                                            <div class="content-header">
                                                <div class="icon">
                                                    <i class="fal fa-life-ring" style="color: red;"></i>
                                                </div>
                                                <h5 class="heading">
                                                    Solutions innovantes                                                </h5>
                                            </div>
                                            <div class="content">
                                                <div class="text">Nos offres de services sont des solutions innovantes basées à sur du cloud afin de réduire les coûts et valoriser le cœur de métier des organisations.
                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 wow move-up">
                                <div class="grid-item animate">
                                    <div class="ht-box-icon style-03">
                                        <div class="icon-box-wrap">
                                            <div class="content-header">
                                                <div class="icon">
                                                    <i class="fal fa-lock-alt" style="color: red;"></i>
                                                </div>
                                                <h5 class="heading">
                                                    Equipes expérimentées
                                                </h5>
                                            </div>
                                            <div class="content">
                                                <div class="text">Une équipe experte mis en oeuvre dans l'accomplissement de sa tâche : vous assistez du début à la fin et même après le déploiement de la solution.
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 wow move-up">
                                <div class="grid-item animate">
                                    <div class="ht-box-icon style-03">
                                        <div class="icon-box-wrap">
                                            <div class="content-header">
                                                <div class="icon">
                                                    <i class="fal fa-globe" style="color: red"></i>
                                                </div>
                                                <h5 class="heading">
                                                    Partenaires de renom
                                                </h5>
                                            </div>
                                            <div class="content">
                                                <div class="text">Ceci reste un véritable atout chez SYTELIS : le choix de nos partenaires de réputation africaine et mondiale  et le haut niveau d'engament pris avec eux.
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 wow move-up">
                                <div class="grid-item animate">
                                    <div class="ht-box-icon style-03">
                                        <div class="icon-box-wrap">
                                            <div class="content-header">
                                                <div class="icon">
                                                    <i class="fal fa-medal" style="color: red;"></i>
                                                </div>
                                                <h5 class="heading">
                                                    Another vision
                                                </h5>
                                            </div>
                                            <div class="content">
                                                <div class="text">D'ici 4 ans, la majorité des entreprises utilisera intensivement des environnements multicloud capables de relever les défis IT les plus pertinents et en toute sécurité. Notre vision est de vous y intégrer.
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--=========== feature-icon-wrapper End =============-->










    <!--========== Call to Action Area Start ============-->
    <div class="cta-image-area_one section-space--ptb_100 cta-bg-image_one">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-xl-5 col-lg-5">
                    <div class="cta-content md-text-center">
                        <h3 class="heading text-white">Avec le Cloud, nous allons simplifier votre vie professionnelle. Le but,  <span
                                class="text-color-secondary" style="color: rgb(91, 235, 91)"> votre succès.</span></h3>
                    </div>
                    <div class="cta-button-group--one text-center">
                        <a href="#" class="btn btn--white btn-one"><span class="btn-icon mr-2"><i
                                    class="far fa-comment-alt-dots"></i></span> Parlons en</a>
                        <a href="#" class="ht-btn ht-btn-md"><span class="btn-icon mr-2"><i
                                    class="far fa-info-circle"></i></span> Plus d'infos</a>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-5">

                </div>
            </div>
        </div>
    </div>
    <!--========== Call to Action Area End ============-->

    <!--====================  brand logo slider area ====================-->
    <div class="brand-logo-slider-area section-space--ptb_60">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <!-- brand logo slider -->
                    <div class="brand-logo-slider__container-area">
                        <div class="swiper-container brand-logo-slider__container">
                            <div class="swiper-wrapper brand-logo-slider__one">
                                <div class="swiper-slide brand-logo brand-logo--slider">
                                    <a href="#">
                                        <div class="brand-logo__image">
                                            <img src="assets/images/logo/partenaires/partenaires8.jpeg" class="img-fluid"
                                                alt="">
                                        </div>
                                        <div class="brand-logo__image-hover">
                                            <img src="assets/images/logo/partenaires/partenaires8.jpeg"
                                                class="img-fluid" alt="">
                                        </div>
                                    </a>
                                </div>
                                <div class="swiper-slide brand-logo brand-logo--slider">
                                    <a href="#">
                                        <div class="brand-logo__image">
                                            <img src="assets/images/logo/partenaires/partenaires7.jpeg" class="img-fluid"
                                                alt="">
                                        </div>
                                        <div class="brand-logo__image-hover">
                                            <img src="assets/images/logo/partenaires/partenaires7.jpeg"
                                                class="img-fluid" alt="">
                                        </div>
                                    </a>
                                </div>
                                <div class="swiper-slide brand-logo brand-logo--slider">
                                    <a href="#">
                                        <div class="brand-logo__image">
                                            <img src="assets/images/logo/partenaires/partenaires3.jpeg" class="img-fluid"
                                                alt="">
                                        </div>
                                        <div class="brand-logo__image-hover">
                                            <img src="assets/images/logo/partenaires/partenaires3.jpeg"
                                                class="img-fluid" alt="">
                                        </div>
                                    </a>
                                </div>
                                <div class="swiper-slide brand-logo brand-logo--slider">
                                    <a href="#">
                                        <div class="brand-logo__image">
                                            <img src="assets/images/logo/partenaires/partenaires6.jpeg" class="img-fluid"
                                                alt="">
                                        </div>
                                        <div class="brand-logo__image-hover">
                                            <img src="assets/images/logo/partenaires/partenaires6.jpeg"
                                                class="img-fluid" alt="">
                                        </div>
                                    </a>
                                </div>
                                <div class="swiper-slide brand-logo brand-logo--slider">
                                    <a href="#">
                                        <div class="brand-logo__image">
                                            <img src="assets/images/logo/partenaires/partenaires2.jpeg" class="img-fluid"
                                                alt="">
                                        </div>
                                        <div class="brand-logo__image-hover">
                                            <img src="assets/images/logo/partenaires/partenaires2.jpeg"
                                                class="img-fluid" alt="">
                                        </div>
                                    </a>
                                </div>
                                <div class="swiper-slide brand-logo brand-logo--slider">
                                    <a href="#">
                                        <div class="brand-logo__image">
                                            <img src="assets/images/logo/partenaires/partenaires1.jpeg" class="img-fluid"
                                                alt="">
                                        </div>
                                        <div class="brand-logo__image-hover">
                                            <img src="assets/images/logo/partenaires/partenaires1.jpeg"
                                                class="img-fluid" alt="">
                                        </div>
                                    </a>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--====================  End of brand logo slider area  ====================-->

</div>




<!--====================  footer area ====================-->

<!--====================  End of footer area  ====================-->


@endsection
